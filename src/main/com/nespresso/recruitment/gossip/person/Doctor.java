package com.nespresso.recruitment.gossip.person;

import java.util.ArrayList;
import java.util.List;

public class Doctor extends Person {

	private List<String> talks= new ArrayList<String>();
	private int indexTalkTransmit=0;

	public Doctor(String name) {
		super(name);
	}

	@Override
	public void receiveTalkFromGossips() {
		for(Person gossip : gossips){
			String talk = gossip.transmitTalk();
			if(!talk.isEmpty())
				this.receiveTalk(talk);
		}
	}


	@Override
	public String transmitTalk() {
		return talks.get(++indexTalkTransmit);
	}

	@Override
	public String ask() {
		StringBuilder builder = new StringBuilder();
		for(String talk : talks){
			builder.append(talk);
			builder.append(", ");
		}
		return builder.delete(builder.length()-2, builder.length()).toString();
	}

	@Override
	public void receiveTalk(String talk) {
		this.talks.add(talk);
	}

}
