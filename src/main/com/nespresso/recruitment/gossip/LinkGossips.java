package com.nespresso.recruitment.gossip;

import com.nespresso.recruitment.gossip.person.Person;

public class LinkGossips implements ToStrategie {
	
	private Person gossip;
	
	public LinkGossips(Person gossip) {
		this.gossip=gossip;
	}

	@Override
	public void To(Person person) {
		person.addGossip(gossip);
	}
	
	

	

}
