package com.nespresso.recruitment.gossip;

import com.nespresso.recruitment.gossip.person.Person;

public class LinkTalk implements ToStrategie {

	private String talk;
	public LinkTalk(String talk) {
		this.talk = talk;
	}

	@Override
	public void To(Person person) {
		person.receiveTalk(talk);
	}

}
