package com.nespresso.recruitment.gossip.person;

import java.util.ArrayList;
import java.util.List;

public abstract class Person {

	private String name;
	protected List<Person> gossips=new ArrayList<Person>();

	public Person(String name) {
		super();
		this.name = name;
	}

	public void addGossip(Person person){
		gossips.add(person);
	}

	public abstract void receiveTalk(String talk);
	public abstract String ask();
	public abstract void receiveTalkFromGossips();
	public abstract String transmitTalk();

}
