package com.nespresso.recruitment.gossip;

import com.nespresso.recruitment.gossip.person.Doctor;
import com.nespresso.recruitment.gossip.person.Mister;
import com.nespresso.recruitment.gossip.person.Person;

public enum PersonFactory {
	INSTANCE;

	public Person createPerson(String personType, String name) {
		switch(personType){
		case "Dr":
			return new Doctor(name);
		case "Mr":
			return new Mister(name);
		}
		return null;
	}

}
