package com.nespresso.recruitment.gossip;

import java.util.HashMap;
import java.util.Map;

import com.nespresso.recruitment.gossip.person.Person;

public class Gossips {

	private Map<String,Person> persons;
	private ToStrategie strategie;
	
	
	public Gossips(String... personsRepresentation) {
		this.persons=createPersons(personsRepresentation);
	}

	private Map<String,Person> createPersons(String... personsRepresentation) {
		Map<String,Person> persons=new HashMap<String, Person>();
		for(String personRepresentation : personsRepresentation){
			String name = PersonParser.parseName(personRepresentation);
			String personType = PersonParser.parseType(personRepresentation);
			Person person = PersonFactory.INSTANCE.createPerson(personType,name);
			persons.put(name,person);
		}
		return persons;
	}

	public Gossips from(String name) {
		Person gossip=persons.get(name);
		strategie=new LinkGossips(gossip);	
		return this;
	}

	public Gossips to(String name) {
		Person person = persons.get(name);
		strategie.To(person);
		return this;
	}

	public Gossips say(String talk) {
		strategie=new LinkTalk(talk);
		return this;
	}

	public String ask(String name) {
		Person person = persons.get(name);
		return person.ask();
	}

	public void spread() {
		for(Person person: persons.values()){
			person.receiveTalkFromGossips();
		}
	}

}
