package com.nespresso.recruitment.gossip.person;

public class Mister extends Person {

	private String talk;

	public Mister(String name) {
		super(name);
		this.talk="";
	}

	@Override
	public void receiveTalkFromGossips() {
		for(Person gossip : gossips){
			String talk = gossip.transmitTalk();
			if(!talk.isEmpty()){
				receiveTalk(talk);
				return;
			}
		}
	}

	@Override
	public String transmitTalk() {
		String copyTalk = new String(this.talk);
		this.talk="";
		return copyTalk;
	}

	@Override
	public String ask() {
		return this.talk;
	}

	@Override
	public void receiveTalk(String talk) {
		this.talk=talk;
	}

}
