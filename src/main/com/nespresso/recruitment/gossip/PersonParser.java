package com.nespresso.recruitment.gossip;

public class PersonParser {

	private static final String PERSON_SEPARATOR = " ";

	public static String parseName(String personRepresentation) {
		return personRepresentation.split(PERSON_SEPARATOR)[1];
	}

	public static String parseType(String personRepresentation) {
		return personRepresentation.split(PERSON_SEPARATOR)[0];
	}

}
